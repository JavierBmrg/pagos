<?php
namespace Pagos\Dao\UsuarioDaoImpl;
# @Author: Edward F Bamberg <javier>
# @Date:   24-Sep-2017-12:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: UsuarioDaoImpl.php
# @Last modified by:   Edward
# @License: gplv2

use Pagos\Repository\Usuario\Usuario;

/**
 * Trait para persistir data contra BD.
 */
trait PersistData{
  /**
   * @return void
   */
  public function executeSaveUsuario($data)
  {
    // En este bloque se ejecuta y organiza query para guardar en BD. No son necesarios según enunciado.
  }
  /**
   * @return integer
   */
  public function executeGetUsuario($id)
  {
    // En este bloque se ejecuta query para obtener id de usuario, para propositos de validación
  }
  /**
   * @return void
   */
  public function executeUpdateUsuario($data,$id)
  {
    // En este bloque se ejecuta la modificación por su respectivo id, obtenido del array pasado por parametro. No son necesarios según enunciado.
  }
  /**
   * @return void
   */
  public function executeDeleteUsuario($id)
  {
    // En este bloque se ejecuta la eliminación por su respectivo id, de función parent:: pasado por parametro. No son necesarios queries según enunciado.
  }
}


/**
 * Clase de implementación - Persistencia de la entidad Usuario.
 */
class UsuarioDaoImpl extends Usuario implements Crudeable
{
  Use PersistData;
  public function __construct($name,$email,$username,$lastname,$id,$age,$gender,$password)
  {

    parent::__construct($name,$email,$username,$lastname,$id,$age,$gender,$password);


    if($this->validateData())
    {
        $this->data = $this->compressData();
    }else {
      throw new Exception('Denied.');
    }

  }
  /**
   * @return void
   */
  public function save()
  {
    $this->executeQuery($this->data);
  }

  /**
   * @return void
   */
  public function update()
  {
    $this->executeUpdate($this->data,$this->getId());
  }

  /**
   * @return void
   */
  public function deleteById()
  {
    $this->executeDelete($this->getId());
  }

  /**
   * @return integer
   */
  public function getUsuario()
  {
    return $this->executeGetUsuario($this->getId());
  }


}
