<?php
# @Author: Edward F Bamberg <javier>
# @Date:   24-Sep-2017-11:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Crudeable.php
# @Last modified by:   Edward
# @License: gplv2
namespace Crudeable;

/**
 * Interfaz para acciones comunes, la implementación la hace cada Repository a su manera en tal caso.
 */
interface Crudeable
{

  public function save();
  public function update();
  public function deleteById();


}
