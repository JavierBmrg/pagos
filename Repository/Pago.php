<?php
namespace Pagos\Repository\Pago;
# @Author: Edward F Bamberg <javier>
# @Date:   24-Sep-2017-1:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Pago.php
# @Last modified by:   Edward
# @License: gplv2


use Pagos\Dao\UsuarioDaoImpl\UsuarioDaoImpl;
use Pagos\Dao\Crudeable\Crudeable;


/**
 * Trait para persistir data contra BD.
 */
trait PersistData{
  /**
   * @return void
   */
  public function executeSavePagoUsuariosPagos($idPago,$idUser,$amount,$date)
  {
   #En este bloque se ejecutan queries para guardar en USUARIOS_PAGOS y PAGOS. No son necesarios según enunciado.
  }
  /**
   * @return void
   */
  public function executeUpdatePago($idPago,$amount,$date)
  {
    // En este bloque se ejecuta la modificación por su respectivo id, obtenido del array pasado por parametro. No son necesarios según enunciado.
  }
  /**
   * @return void
   */
  public function executeDeletePago($idPago)
  {
    // En este bloque se ejecuta la eliminación por su respectivo id, de función parent:: pasado por parametro. No son necesarios queries según enunciado.
  }
}


/**
 * Clase Pago
 */
class Pago extends UsuarioDaoImpl implements Crudeable
{
  use PersistData;

  private $pagoId;
  private $amount;
  private $date;
  private $instance;


  public function __construct($pagoId,$amount,$date, UsuarioDaoImpl $instanceUsuarioDaoImpl)
  {
    $this->pagoId = $pagoId;
    $this->amount = $amount;
    $this->date = $date;
    $this->instance = $instanceUsuarioDaoImpl;
  }

  /**
   * @return bool
   */
  public function validateDate()
  {
   return (strtotime($this->date) === strtotime('today')) ? true : false;
  }

  /**
   * @return bool
   */
  public function validatePago()
  {

    if ($this->amount <= 0 && $this->validateDate())
    {
      return true;
    }else {
      return false;
    }

  }

  /**
   * @return void
   */
  public function save()
  {
    if ($this->validatePago()) {
      $this->executeSavePagoUsuariosPagos($this->pagoId,$this->instance->getUsuario(),$this->amount,$this->date);
    }
    else {
      throw new Exception('Error con datos de pago.');
    }
  }

  /**
   * @return void
   */
  public function update()
  {
    executeUpdatePago($this->pagoId,$this->amount,$this->date);
  }

  /**
   * @return void
   */
  public function deleteById()
  {
    executeDeletePago($this->pagoId);
  }


}
