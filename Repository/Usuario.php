<?php
namespace Pagos\Repository\Usuario;
# @Author: Edward F Bamberg <javier>
# @Date:   24-Sep-2017-1:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Usuario.php
# @Last modified by:   Edward
# @License: gplv2


/**
 * Clase para Usuario.
 */
class Usuario
{

  private $name;
  private $lastname;
  private $email;
  public  $username;
  private $id;
  private $age;
  private $gender;
  private $password;
  public  $data;

  const MAX_AGE = 18;

  public function __construct($name,$email,$username,$lastname,$id,$age,$gender,$password)
  {
    $this->name = $name;
    $this->email = $email;
    $this->useranme = $username;
    $this->lastname = $lastname;
    $this->id = $id;
    $this->age = $age;
    $this->gender = $gender;
    $this->password = $password;
    $this->data = array();

    if($this->validateData())
    {
      $this->compressData();
    }
    else {
      throw new Exception('Error construct Usuario class.');
    }
  }

  /**
   * @return bool
   */
  public function validateData()
  {
    return (empty($this->id) || $this->age < self::MAX_AGE) ? false : true;
  }

  /**
   * @return array
   */
  public function compressData()
  {
    array_push($this->data,$this->id,$this->username,$this->name,$this->email,$this->lastname,$this->age,$this->gender,$this->password);

    return $this->data;
  }

  /**
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return void
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getUserName()
  {
    $this->username = $username;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @return void
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * @return integer
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @return integer
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @return integer
   */
  public function getLastName()
  {
    return $this->lastname;
  }

  /**
   * @return integer
   */
  public function getAge()
  {
    return $this->age;
  }

  /**
   * @return integer
   */
  public function getGender()
  {
    return $this->gender;
  }



}
