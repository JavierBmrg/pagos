<?php
namespace Pagos\Repository\Favorito;
# @Author: Edward F Bamberg <javier>
# @Date:   24-Sep-2017-1:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Favorito.php
# @Last modified by:   Edward
# @License: gplv2
use Pagos\Dao\UsuarioDaoImpl\UsuarioDaoImpl;
use Pagos\Dao\Crudeable\Crudeable;

/**
 * Trait para persistir data contra BD.
 */
trait PersistData{
  public function executeSaveFavorito($id,$idFav)
  {
   #En este bloque se ejecutan queries para guardar en USUARIOS_PAGOS y PAGOS. No son necesarios según enunciado.
  }
  public function executeUpdateFavorito($id,$idFav)
  {
    // En este bloque se ejecuta la modificación por su respectivo id, obtenido del array pasado por parametro. No son necesarios según enunciado.
  }
  public function executeDeleteFavorito($idFav)
  {
    // En este bloque se ejecuta la eliminación por su respectivo id, de función parent:: pasado por parametro. No son necesarios queries según enunciado.
  }
}

/**
 * Clase Favorito
 */
class Favorito extends UsuarioDaoImpl implements Crudeable
{
  Use PersistData;

  private $id;
  private $idUsuarioFav;
  public $instance;

  public function __construct($id,$idUsuarioFav, UsuarioDaoImpl $instanceUsuarioDaoImpl)
  {
    $this->$idUsuarioFav = $idUsuarioFav;
    $this->instance = $instanceUsuarioDaoImpl;
    $this->id = $this->instance->getUsuario();
  }


  /**
   * @return void
   */
  public function save()
  {
    $this->executeSaveFavorito($this->id,$this->idUsuarioFav);
  }

  /**
   * @return void
   */
  public function update()
  {
    $this->executeUpdateFavorito($this->id,$this->idUsuarioFav);
  }

  /**
   * @return void
   */
  public function deleteById()
  {
    executeDeleteFavorito($this->id);
  }

  /**
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return integer
   */
  public function getUsuarioFav()
  {
    return $this->idUsuarioFav;
  }

}
